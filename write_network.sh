#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Please specify the target ip"
    exit 1
fi

[ ! -e "pipe" ] && mkfifo pipe

cat pipe | nc $1 1234 &

./build/streamer

