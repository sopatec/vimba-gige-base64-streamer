CFLAGS = -I include -I libs/base64/include
LDLIBS = -lVimbaC -lVimbaImageTransform

BASE64_OBJ = libs/base64/lib/libbase64.o

TEST_OBJS = build/test.o build/AsynchronousGrab.o build/PrintVimbaVersion.o build/ErrorCodeToMessage.o build/DiscoverGigECameras.o
STREAMER_OBJS = build/streamer.o build/AsynchronousGrab.o build/PrintVimbaVersion.o build/ErrorCodeToMessage.o build/DiscoverGigECameras.o $(BASE64_OBJ)

.PHONY: all clean

all: build/streamer build/test

clean:
	rm -rf build

build:
	mkdir build

libs/base64/bin/base64.o:
	make -C libs/base64

build/%.o: src/%.c include/* | build
	$(CC) -c -o $@ $< $(CFLAGS) $(LDLIBS)

build/streamer: $(STREAMER_OBJS) include/* | build
	$(CC) -o $@ $(STREAMER_OBJS) $(CFLAGS) $(LDLIBS)

build/test: $(TEST_OBJS) include/* | build
	$(CC) -o $@ $(TEST_OBJS) $(CFLAGS) $(LDLIBS)
