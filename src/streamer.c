/* REF: AsynchronousGrab Example source (licensed!) */

#include <AsynchronousGrab.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "ErrorCodeToMessage.h"
#include "VimbaIT/VmbTransform.h"
#include "libbase64.h"

int b64out;

int
write_exact(int sockfd, void* target, size_t amount)
{
	size_t totalread = 0, want = amount;
	ssize_t got;

	while ((got = write(sockfd, target + totalread, want)) > 0) {
		if (got == want) return EXIT_SUCCESS;
		totalread += got;
		want = amount - totalread;
	}

	return EXIT_FAILURE;
}

VmbError_t ProcessFrame(VmbFrame_t* pFrame) {
	VmbError_t Result = VmbErrorSuccess;  // result of function
	VmbUint32_t Width = 0;                // will later hold the frame width
	VmbUint32_t Height = 0;               // will later hold the frame height
	VmbImage SourceImage;  // source image struct to pass to image transform
	VmbImage DestinationImage;  // destination image struct to pass to image
								// transform
	VmbRGB8_t* DestinationBuffer = NULL;  // destination image buffer
	VmbTransformInfo TransformInfo;  // if desired the transform information is
									 // constructed here
	VmbUint32_t TransformInfoCount = 0;  // if color processing is desired

	// check if we can get data
	if (NULL == pFrame || NULL == pFrame->buffer) {
		printf("%s error invalid frame\n", __FUNCTION__);
		return VmbErrorBadParameter;
	}

	// init local variables for frame width and height
	Width = pFrame->width;
	Height = pFrame->height;
	if (g_bEnableColorProcessing ==
		VmbBoolTrue)  // if color processing is desired set the transform matrix
	{
		const static VmbFloat_t matrix[9] = {
			0.0, 0.0, 1.0,  // matrix to swap red and blue component
			0.0, 1.0, 0.0, 1.0, 0.0, 0.0};
		Result = VmbSetColorCorrectionMatrix3x3(
			matrix, &TransformInfo);  // initialize transform info
		if (VmbErrorSuccess != Result) {
			printf("%s error could not set transform matrix; Error: %d\n",
				   __FUNCTION__, Result);
			return Result;
		}
		TransformInfoCount = 1;
	}

	// set the struct size to image
	SourceImage.Size = sizeof(SourceImage);

	// set the image information from the frames pixel format and size
	Result = VmbSetImageInfoFromPixelFormat(pFrame->pixelFormat, Width, Height,
											&SourceImage);
	if (VmbErrorSuccess != Result) {
		printf("%s error could not set source image info; Error: %d\n",
			   __FUNCTION__, Result);
		return Result;
	}

	// the frame buffer will be the images data buffer
	SourceImage.Data = pFrame->buffer;

	// set size for destination image
	DestinationImage.Size = sizeof(DestinationImage);

	// set destination image info from frame size and string for RGB8 (rgb24)
	Result =
		VmbSetImageInfoFromString("RGB8", 4, Width, Height, &DestinationImage);
	if (VmbErrorSuccess != Result) {
		printf("%s error could not set destination image info; Error: %d\n",
			   __FUNCTION__, Result);
		return Result;
	}

	// allocate buffer for destination image size is width * height * size of
	// rgb pixel
	DestinationBuffer = (VmbRGB8_t*)malloc(Width * Height * sizeof(VmbRGB8_t));
	if (NULL == DestinationBuffer) {
		printf(
			"%s error could not allocate rgb buffer for width: %d and height: "
			"%d\n",
			__FUNCTION__, Width, Height);
		return VmbErrorResources;
	}

	// set the destination buffer to the data buffer of the image
	DestinationImage.Data = DestinationBuffer;

	// transform source to destination if color processing was enabled
	// TransformInfoCount is 1 otherwise TransformInfo will be ignored
	Result = VmbImageTransform(&SourceImage, &DestinationImage, &TransformInfo,
							   TransformInfoCount);

	// allocate memory for base64 encoded text
	char metadata[256];
	void *encoded = malloc(Width * Height * sizeof(VmbRGB8_t) * 2);
	size_t output_size = 0, written = 0, metalen;
	base64_encode((const char *) DestinationBuffer, Width * Height * sizeof(VmbRGB8_t),
			encoded, &output_size, 0);

	metalen = snprintf(metadata, 256, "%u,%u,1", Width, Height);
	if (metalen <= 0) {
		printf("Writing metadata failed..\n");
		exit(EXIT_FAILURE);
	}

	// write data to stream
	//printf("Input size: %li\n", Width * Height * sizeof(VmbRGB8_t));
	//printf("Ouput size: %li\n", output_size);
	if (write_exact(b64out, metadata, metalen + 1) != EXIT_SUCCESS
			|| write_exact(b64out, encoded, output_size) != EXIT_SUCCESS
			|| write_exact(b64out, "\n", 1) != EXIT_SUCCESS) {
		printf("Write to pipe failed.. (%i) %s\n", errno, strerror(errno));
		exit(EXIT_FAILURE);
	}

	// free base64 text
	free(encoded);

	// free image buffer
	free(DestinationBuffer);
	return -Result;
}

void VMB_CALL FrameCallback(const VmbHandle_t cameraHandle,
							VmbFrame_t* pFrame) {
	//
	// from here on the frame is under user control until returned to Vimba by
	// re queuing it if you want to have smooth streaming keep the time you hold
	// the frame short
	//
	VmbBool_t bShowFrameInfos = VmbBoolFalse;   // showing frame infos
	double dFPS = 0.0;                         // frames per second calculated
	VmbBool_t bFPSValid =
		VmbBoolFalse;                // indicator if fps calculation was valid
	double dFrameTime = 0.0;         // reference time for frames
	double dTimeDiff = 0.0;          // time difference between frames
	VmbUint64_t nFramesMissing = 0;  // number of missing frames

	// Ensure that a frame callback is not interrupted by a VmbFrameRevoke
	// during shutdown
	AquireApiLock();

	if (FrameInfos_Off != g_eFrameInfos) {
		if (FrameInfos_Show == g_eFrameInfos) {
			bShowFrameInfos = VmbBoolTrue;
		}

		if (VmbFrameFlagsFrameID & pFrame->receiveFlags) {
			if (g_bFrameIDValid) {
				if (pFrame->frameID != (g_nFrameID + 1)) {
					// get difference between current frame and last received
					// frame to calculate missing frames
					nFramesMissing = pFrame->frameID - g_nFrameID - 1;
					if (1 == nFramesMissing) {
						printf("%s 1 missing frame detected\n", __FUNCTION__);
					} else {
						printf("%s error %llu missing frames detected\n",
							   __FUNCTION__, nFramesMissing);
					}
				}
			}
			g_nFrameID =
				pFrame->frameID;  // store current frame id to calculate missing
								  // frames in the next calls
			g_bFrameIDValid = VmbBoolTrue;

			dFrameTime =
				GetTime();  // get current time to calculate frames per second
			if ((g_bFrameTimeValid)        // only if the last time was valid
				&& (0 == nFramesMissing))  // and the frame is not missing
			{
				dTimeDiff = dFrameTime - g_dFrameTime;  // build time difference
														// with last frames time
				if (dTimeDiff > 0.0) {
					dFPS = 1.0 / dTimeDiff;
					bFPSValid = VmbBoolTrue;
				} else {
					bShowFrameInfos = VmbBoolTrue;
				}
			}
			// store time for fps calculation in the next call
			g_dFrameTime = dFrameTime;
			g_bFrameTimeValid = VmbBoolTrue;
		} else {
			bShowFrameInfos = VmbBoolTrue;
			g_bFrameIDValid = VmbBoolFalse;
			g_bFrameTimeValid = VmbBoolFalse;
		}
		// test if the frame is complete
		if (VmbFrameStatusComplete != pFrame->receiveStatus) {
			bShowFrameInfos = VmbBoolTrue;
		}
	}

	if (bShowFrameInfos) {
		printf("Frame ID:");
		if (VmbFrameFlagsFrameID & pFrame->receiveFlags) {
			printf("%llu", pFrame->frameID);
		} else {
			printf("?");
		}

		printf(" Status:");
		switch (pFrame->receiveStatus) {
			case VmbFrameStatusComplete:
				printf("Complete");
				break;

			case VmbFrameStatusIncomplete:
				printf("Incomplete");
				break;

			case VmbFrameStatusTooSmall:
				printf("Too small");
				break;

			case VmbFrameStatusInvalid:
				printf("Invalid");
				break;

			default:
				printf("?");
				break;
		}

		printf(" Size:");
		if (VmbFrameFlagsDimension & pFrame->receiveFlags) {
			printf("%ux%u", pFrame->width, pFrame->height);
		} else {
			printf("?x?");
		}

		printf(" Format:0x%08X", pFrame->pixelFormat);

		printf(" FPS:");
		if (bFPSValid) {
			printf("%.2f", dFPS);
		} else {
			printf("?");
		}

		printf("\n");
	}

	if (g_bRGBValue) {
		// goto image processing
		ProcessFrame(pFrame);
	} else if (FrameInfos_Show != g_eFrameInfos) {
		// Print a dot every frame
		printf(".");
	}

	fflush(stdout);
	// requeue the frame so it can be filled again
	VmbCaptureFrameQueue(cameraHandle, pFrame, &FrameCallback);

	ReleaseApiLock();
}

int main(int argc, char* argv[]) {
	VmbError_t err = VmbErrorSuccess;

	char* pCameraID = NULL;                   // The ID of the camera to use
	FrameInfos eFrameInfos = FrameInfos_Show;  // Show frame infos
	VmbBool_t bRGBValue = VmbBoolTrue;       // Show RGB values
	VmbBool_t bEnableColorProcessing =
		VmbBoolFalse;              // Enables color processing of frames
	unsigned char bPrintHelp = 0;  // Output help?
	int i = 0;                     // Counter for some iteration
	char* pParameter = NULL;       // The command line parameter
	CreateApiLock();

	for (i = 1; i < argc; ++i) {
		pParameter = argv[i];
		if (0 > strlen(pParameter)) {
			err = VmbErrorBadParameter;
			break;
		}

		if ('/' == pParameter[0]) {
			if (0 == strcmp(pParameter, "/i")) {
				if ((FrameInfos_Off != eFrameInfos) || (bPrintHelp)) {
					err = VmbErrorBadParameter;
					break;
				}

				eFrameInfos = FrameInfos_Show;
			} else if (0 == strcmp(pParameter, "/r")) {
				if (bPrintHelp) {
					err = VmbErrorBadParameter;
					break;
				}

				bRGBValue = VmbBoolTrue;
			} else if (0 == strcmp(pParameter, "/c")) {
				if (bPrintHelp) {
					err = VmbErrorBadParameter;
					break;
				}

				bEnableColorProcessing = VmbBoolTrue;
				bRGBValue = VmbBoolTrue;
			} else if (0 == strcmp(pParameter, "/a")) {
				if ((FrameInfos_Off != eFrameInfos) || (bPrintHelp)) {
					err = VmbErrorBadParameter;
					break;
				}

				eFrameInfos = FrameInfos_Automatic;
			} else if (0 == strcmp(pParameter, "/h")) {
				if ((NULL != pCameraID) || (bPrintHelp) ||
					(VmbBoolFalse != bEnableColorProcessing) ||
					(VmbBoolFalse != bRGBValue) ||
					(FrameInfos_Off != eFrameInfos)) {
					err = VmbErrorBadParameter;
					break;
				}

				bPrintHelp = 1;
			} else {
				err = VmbErrorBadParameter;
				break;
			}
		} else {
			if (NULL != pCameraID) {
				err = VmbErrorBadParameter;
				break;
			}

			pCameraID = pParameter;
		}
	}

	// Write out an error if we could not parse the command line
	if (VmbErrorBadParameter == err) {
		printf("Invalid parameters!\n\n");
		bPrintHelp = 1;
	}

	b64out = open(argc > 1 ? argv[1] : "pipe", O_WRONLY);
	if (b64out < 0) {
		printf("Connection to pipe failed..\n"
			"Reader needs to be listening before writer is started\n");
		return EXIT_FAILURE;
	}

	// Print out help and end program
	if (bPrintHelp) {
		printf("Usage: AsynchronousGrab [CameraID] [/i] [/h]\n");
		printf("Parameters:   CameraID    ID of the camera to use (using first camera if not specified)\n");
		printf("              /r          Convert to RGB and show RGB values\n");
		printf("              /c          Enable color processing (includes /r)\n");
		printf("              /i          Show frame infos\n");
		printf("              /a          Automatically only show frame infos of corrupt frames\n");
		printf("              /h          Print out help\n");
	} else {
		err = StartContinuousImageAcquisition(
			pCameraID, eFrameInfos, bEnableColorProcessing, bRGBValue, FrameCallback);
		if (VmbErrorSuccess == err) {
			printf("Press <enter> to stop acquisition...\n");
			getchar();

			StopContinuousImageAcquisition();
		}

		if (VmbErrorSuccess == err) {
			printf("\nAcquisition stopped.\n");
		} else {
			printf("\nAn error occurred: %s\n", ErrorCodeToMessage(err));
		}
	}

	ReleaseApiLock();
	return err;
}
