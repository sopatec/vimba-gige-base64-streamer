/* REF: modfied AsynchronousGrab (licensed!) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include <AsynchronousGrab.h>
#include <VimbaC/Include/VimbaC.h>

#include "DiscoverGigECameras.h"
#include "PrintVimbaVersion.h"
#include "VimbaIT/VmbTransform.h"


VmbBool_t g_bVimbaStarted = VmbBoolFalse;  // Remember if Vimba is started
VmbBool_t g_bStreaming = VmbBoolFalse;     // Remember if Vimba is streaming
VmbBool_t g_bAcquiring = VmbBoolFalse;     // Remember if Vimba is acquiring
VmbHandle_t g_CameraHandle = NULL;         // A handle to our camera
VmbFrame_t g_Frames[NUM_FRAMES];           // The frames we capture into
FrameInfos g_eFrameInfos =
	FrameInfos_Off;  // Remember if we should print out frame infos
VmbBool_t g_bRGBValue = VmbBoolFalse;  // Show RGB values
VmbBool_t g_bEnableColorProcessing =
	VmbBoolFalse;           // Enables color processing for frames
double g_dFrameTime = 0.0;  // Timestamp of last frame
VmbBool_t g_bFrameTimeValid =
	VmbBoolFalse;            // Remember if there was a last timestamp
VmbUint64_t g_nFrameID = 0;  // ID of last frame
VmbBool_t g_bFrameIDValid = VmbBoolFalse;  // Remember if there was a last ID

pthread_mutex_t g_Mutex = PTHREAD_MUTEX_INITIALIZER;
VmbBool_t CreateApiLock() { return VmbBoolTrue; }
void DestroyApiLock() {}
VmbBool_t AquireApiLock() {
	if (0 == pthread_mutex_lock(&g_Mutex)) {
		return VmbBoolTrue;
	}
	return VmbBoolFalse;
}
void ReleaseApiLock() { pthread_mutex_unlock(&g_Mutex); }

//
// Method: GetTime
//
// Purpose: get time indicator
//
// Returns: time indicator in seconds for differential measurements
double GetTime() {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	return ((double)now.tv_sec) + ((double)now.tv_nsec) / 1000000000.0;
}

//
// Method StartContinuousImageAcquisition
//
// Purpose: starts image acquisition on a given camera
//
// Parameters:
//
// [in]     pCameraId               zero terminated C string with the camera id
// for the camera to be used [in]     eFrameInfos             enumeration value
// for the frame infos to show for received frames [in] bEnableColorProcessing
// toggle for enabling image processing, in this case just swapping red with
// blue
//
// Note: Vimba has to be uninitialized and the camera has to allow access mode
// full
//
VmbError_t StartContinuousImageAcquisition(const char* pCameraID,
										   FrameInfos eFrameInfos,
										   VmbBool_t bEnableColorProcessing,
										   VmbBool_t bRGBValue,
										   void VMB_CALL (* FrameCallback)
										   (const VmbHandle_t cameraHandle,
											VmbFrame_t* pFrame)
										   ) {
	VmbError_t err = VmbErrorSuccess;  // The function result
	VmbCameraInfo_t* pCameras = NULL;  // A list of camera details
	VmbUint32_t nCount = 0;            // Number of found cameras
	VmbUint32_t nFoundCount = 0;       // Change of found cameras
	VmbAccessMode_t cameraAccessMode =
		VmbAccessModeFull;  // We open the camera with full access
	VmbBool_t bIsCommandDone =
		VmbBoolFalse;             // Has a command finished execution
	VmbInt64_t nPayloadSize = 0;  // The size of one frame
	int i = 0;                    // Counting variable

	if (!g_bVimbaStarted) {
		// initialize global state
		g_bStreaming = VmbBoolFalse;
		g_bAcquiring = VmbBoolFalse;
		g_CameraHandle = NULL;
		memset(g_Frames, 0, sizeof(g_Frames));
		g_dFrameTime = 0.0;
		g_bFrameTimeValid = VmbBoolFalse;
		g_nFrameID = 0;
		g_bFrameIDValid = VmbBoolFalse;
		g_eFrameInfos = eFrameInfos;
		g_bRGBValue = bRGBValue;
		g_bEnableColorProcessing = bEnableColorProcessing;

		// Startup Vimba
		err = VmbStartup();
		// Print the version of Vimba
		PrintVimbaVersion();
		if (VmbErrorSuccess == err) {
			g_bVimbaStarted = VmbBoolTrue;

			// Is Vimba connected to a GigE transport layer?
			DiscoverGigECameras();

			// If no camera ID was provided use the first camera found
			if (NULL == pCameraID) {
				// Get the amount of known cameras
				err = VmbCamerasList(NULL, 0, &nCount, sizeof *pCameras);
				if (VmbErrorSuccess == err && 0 != nCount) {
					pCameras =
						(VmbCameraInfo_t*)malloc(nCount * sizeof(*pCameras));
					if (NULL != pCameras) {
						// Actually query all static details of all known
						// cameras without having to open the cameras If a new
						// camera was connected since we queried the amount of
						// cameras (nFoundCount > nCount) we can ignore that one
						err = VmbCamerasList(pCameras, nCount, &nFoundCount,
											 sizeof *pCameras);
						if (VmbErrorSuccess != err && VmbErrorMoreData != err) {
							printf(
								"%s Could not list cameras. Error code: %d\n",
								__FUNCTION__, err);
						} else {
							// Use the first camera
							if (nFoundCount != 0) {
								pCameraID = pCameras[0].cameraIdString;
							} else {
								err = VmbErrorNotFound;
								printf("%s camera lost. Error code: %d\n",
									   __FUNCTION__, err);
								pCameraID = NULL;
							}
						}

						free(pCameras);
						pCameras = NULL;
					} else {
						printf("%s Could not allocate camera list.\n",
							   __FUNCTION__);
					}
				} else {
					printf(
						"%s Could not list cameras or no cameras present. "
						"Error code: %d\n",
						__FUNCTION__, err);
				}
			}

			if (NULL != pCameraID) {
				// Open camera
				err = VmbCameraOpen(pCameraID, cameraAccessMode, &g_CameraHandle);
				if (VmbErrorSuccess == err) {
					printf("Opening camera with ID: %s\n", pCameraID);

					// Set the GeV packet size to the highest possible value
					// (In this example we do not test whether this cam actually
					// is a GigE cam)
					if (VmbErrorSuccess ==
						VmbFeatureCommandRun(g_CameraHandle,
											 "GVSPAdjustPacketSize")) {
						do {
							if (VmbErrorSuccess !=
								VmbFeatureCommandIsDone(g_CameraHandle,
														"GVSPAdjustPacketSize",
														&bIsCommandDone)) {
								break;
							}
						} while (VmbBoolFalse == bIsCommandDone);
					}

					if (VmbErrorSuccess == err) {
						// Evaluate frame size
						err = VmbFeatureIntGet(g_CameraHandle, "PayloadSize",
											   &nPayloadSize);
						if (VmbErrorSuccess == err) {
							for (i = 0; i < NUM_FRAMES; i++) {
								g_Frames[i].buffer = (unsigned char*)malloc(
									(VmbUint32_t)nPayloadSize);
								if (NULL == g_Frames[i].buffer) {
									err = VmbErrorResources;
									break;
								}
								g_Frames[i].bufferSize =
									(VmbUint32_t)nPayloadSize;

								// Announce Frame
								err = VmbFrameAnnounce(
									g_CameraHandle, &g_Frames[i],
									(VmbUint32_t)sizeof(VmbFrame_t));
								if (VmbErrorSuccess != err) {
									free(g_Frames[i].buffer);
									memset(&g_Frames[i], 0, sizeof(VmbFrame_t));
									break;
								}
							}

							if (VmbErrorSuccess == err) {
								// Start Capture Engine
								err = VmbCaptureStart(g_CameraHandle);
								if (VmbErrorSuccess == err) {
									g_bStreaming = VmbBoolTrue;
									for (i = 0; i < NUM_FRAMES; i++) {
										// Queue Frame
										err = VmbCaptureFrameQueue(
											g_CameraHandle, &g_Frames[i],
											FrameCallback);
										if (VmbErrorSuccess != err) {
											break;
										}
									}

									if (VmbErrorSuccess == err) {
										// Start Acquisition
										err = VmbFeatureCommandRun(
											g_CameraHandle, "AcquisitionStart");
										if (VmbErrorSuccess == err) {
											g_bAcquiring = VmbBoolTrue;
										}
									}
								}
							}
						}
					}
				}
			}

			if (VmbErrorSuccess != err) {
				StopContinuousImageAcquisition();
			}
		}
	} else {
		err = VmbErrorOther;
	}

	return err;
}

//
// Method: StopContinuousImageAcquisition
//
// Purpose: stops image acquisition that was started with
// StartContinuousImageAcquisition
//
void StopContinuousImageAcquisition() {
	int i = 0;

	if (g_bVimbaStarted) {
		if (NULL != g_CameraHandle) {
			if (g_bAcquiring) {
				// Stop Acquisition
				VmbFeatureCommandRun(g_CameraHandle, "AcquisitionStop");
				g_bAcquiring = VmbBoolFalse;
			}

			if (g_bStreaming) {
				// Stop Capture Engine
				VmbCaptureEnd(g_CameraHandle);
				g_bStreaming = VmbBoolFalse;
			}

			// Flush the capture queue
			VmbCaptureQueueFlush(g_CameraHandle);

			// Ensure that revoking is not interrupted by a dangling frame
			// callback
			AquireApiLock();
			for (i = 0; i < NUM_FRAMES; i++) {
				if (NULL != g_Frames[i].buffer) {
					VmbFrameRevoke(g_CameraHandle, &g_Frames[i]);
					free(g_Frames[i].buffer);
					memset(&g_Frames[i], 0, sizeof(VmbFrame_t));
				}
			}
			ReleaseApiLock();
			// Close camera
			VmbCameraClose(g_CameraHandle);
			g_CameraHandle = NULL;
		}
		VmbShutdown();
		g_bVimbaStarted = VmbBoolFalse;
		pthread_mutex_destroy(&g_Mutex);
	}
}

