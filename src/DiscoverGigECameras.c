/* REF: modified DiscoverGigECameras (licensed) */

#include "DiscoverGigECameras.h"

#include <VimbaC/Include/VimbaC.h>
#include <stdio.h>
#include <unistd.h>

// Purpose: Discovers GigE cameras if GigE TL is present.
//          Discovery is switched on only once so that the API can detect all
//          currently connected cameras.
VmbError_t DiscoverGigECameras() {
	VmbError_t err = VmbErrorSuccess;
	VmbBool_t isGigE = VmbBoolFalse;

	err = VmbFeatureBoolGet(
		gVimbaHandle, "GeVTLIsPresent",
		&isGigE);  // Is Vimba connected to a GigE transport layer?
	if (VmbErrorSuccess == err) {
		if (VmbBoolTrue == isGigE) {
			err = VmbFeatureIntSet(
				gVimbaHandle, "GeVDiscoveryAllDuration",
				250);  // Set the waiting duration for discovery packets to
					   // return. If not set the default of 150 ms is used.
			if (VmbErrorSuccess == err) {
				err = VmbFeatureCommandRun(
					gVimbaHandle,
					"GeVDiscoveryAllOnce");  // Send discovery packets to GigE
											 // cameras and wait 250 ms until
											 // they are answered
				if (VmbErrorSuccess != err) {
					printf(
						"Could not ping GigE cameras over the network. Reason: "
						"%d\n",
						err);
				}
			} else {
				printf(
					"Could not set the discovery waiting duration. Reason: "
					"%d\n",
					err);
			}
		}
	} else {
		printf(
			"Could not query Vimba for the presence of a GigE transport layer. "
			"Reason: %d\n\n",
			err);
	}

	return err;
}
