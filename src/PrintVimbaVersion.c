/* REF: modified PrintVimbaVersion (licensed!) */

#include "PrintVimbaVersion.h"

#include <VimbaC/Include/VimbaC.h>
#include <stdio.h>

//
// Prints out the version of the Vimba API
//
void PrintVimbaVersion() {
	VmbVersionInfo_t version_info;
	VmbError_t result = VmbVersionQuery(&version_info, sizeof(version_info));
	if (VmbErrorSuccess == result) {
		printf("Vimba Version Major: %u Minor: %u Patch: %u\n",
			   version_info.major, version_info.minor, version_info.patch);
	} else {
		printf("VmbVersionQuery failed with Reason: %x\n", result);
	}
}

