/* REF: modified ErrorCodeToMessage (licensed!) */

#ifndef ERROR_CODE_TO_MESSAGE_H_
#define ERROR_CODE_TO_MESSAGE_H_

#include "VimbaC/Include/VimbaC.h"

//
// Translates Vimba error codes to readable error messages
//
// Parameters:
//  [in]    eError      The error code to be converted to string
//
// Returns:
//  A descriptive string representation of the error code
//
const char* ErrorCodeToMessage(VmbError_t eError);

#endif
