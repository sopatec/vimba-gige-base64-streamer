/* REF: modified AsynchronousGrab (licensed!) */

#ifndef ASYNCHRONOUS_GRAB_H_
#define ASYNCHRONOUS_GRAB_H_

#include <VimbaC/Include/VmbCommonTypes.h>
#include <VimbaC/Include/VimbaC.h>

typedef enum FrameInfos {
	FrameInfos_Off,
	FrameInfos_Show,
	FrameInfos_Automatic
} FrameInfos;

VmbError_t StartContinuousImageAcquisition(const char* pCameraID,
										   FrameInfos eFrameInfos,
										   VmbBool_t bEnableColorProcessing,
										   VmbBool_t bRGBValue,
										   void VMB_CALL (* FrameCallback)
										   (const VmbHandle_t cameraHandle,
											VmbFrame_t* pFrame));
void StopContinuousImageAcquisition();

VmbBool_t CreateApiLock();
void DestroyApiLock();
VmbBool_t AquireApiLock();
void ReleaseApiLock();

double GetTime();


enum { NUM_FRAMES = 3 };

extern VmbBool_t g_bVimbaStarted;
extern VmbBool_t g_bStreaming;
extern VmbBool_t g_bAcquiring;
extern VmbHandle_t g_CameraHandle;
extern VmbFrame_t g_Frames[NUM_FRAMES];           // The frames we capture into
extern FrameInfos g_eFrameInfos;
extern VmbBool_t g_bRGBValue;
extern VmbBool_t g_bEnableColorProcessing;
extern double g_dFrameTime;
extern VmbBool_t g_bFrameTimeValid;
extern VmbUint64_t g_nFrameID;
extern VmbBool_t g_bFrameIDValid;

#endif

