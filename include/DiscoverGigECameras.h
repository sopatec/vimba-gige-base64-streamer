/* REF: modified DiscoverGigECameras (licensed!) */

#ifndef DISCOVER_GIGE_CAMERAS_H_
#define DISCOVER_GIGE_CAMERAS_H_

#include <VimbaC/Include/VimbaC.h>

// Purpose: Discovers GigE cameras if GigE TL is present.
//          Discovery is switched on only once so that the API can detect all
//          currently connected cameras.
VmbError_t DiscoverGigECameras();

#endif
