/* REF: modified PrintVimbaVersion (licensed!) */

#ifndef PRINT_VIMBA_VERSION_H_
#define PRINT_VIMBA_VERSION_H_

//
// Prints out the version of the Vimba API
//
void PrintVimbaVersion();

#endif
